'''
Guide line for testing

The file test file should be placed in the test folder and should be named following the convention : "test_*.py".

The command to test the library is (you have to be in N2D2/python) :

python -m unittest discover -s test -v

The option discovery check for test_*.py files, so for example this file will note be caught !


If you need more information please check : https://docs.python.org/3/library/unittest.html
'''

import unittest

class test_name(unittest.TestCase):
    """
    The class needs to inherit unittest.TestCase, the name doesn't matter and the class doesn't need to be instantiated.
    """

    def setUp(self):
        """
        Method called before each test
        """
        pass

    def tearDown(self):
        """
        Method called after a test, even if it failed.
        Can be used to clean variables
        """
        pass

    def test_X(self):
        """
        Method called to test a functionality. It needs to be named test_* to be called.
        """

        """
        To test the functions you can use one of the following method :
        - self.assertEqual(a, b)
        - self.assertNotEqual(a, b)
        - self.assertTrue(a)
        - self.assertFalse(a)
        - self.assertIs(a, b)
        - self.assertIsNot(a, b)
        - self.assertIsNotNone(a)
        - self.assertIn(a, b)
        - self.assertNotIn(a, b)
        - self.assertIsInstance(a, b)
        """

        """
        You can use the following decorator : 
        - @unittest.skip(display_text)
        - @unittest.skipIf(cond, display_text)
        - @unittest.skipUnless(cond, display_text)
        - @unittest.expectedFailure()
        """

        """
        You can test that a function raises error by putting it in a block :
        with self.assertRaises(TypeError): 
        You can replace TypeError by the type of message you are expecting
        """
        pass

if __name__ == '__main__':
    """
    You need to add this line for the tests to be run.
    """
    unittest.main()